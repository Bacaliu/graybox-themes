;;; graybox-theme.el --- Theme

;; Copyright (C) 2023 , Adrian Bacaliu

;; Author: Adrian Bacaliu
;; Version: 0.1
;; Package-Requires: ((emacs "24.1"))
;; Created with ThemeCreator, https://github.com/mswift42/themecreator.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:
;;; graybox theme created by Adrian Bacaliu in 2023

;;; Code:

(defun graybox-set-faces (theme-name)
  "Actually make the colors.  THEME-NAME is a symbol like graybox-light."

  (custom-theme-set-faces
   theme-name
   `(success ((,class (:foreground ,accent))))
   `(error ((,class (:foreground ,error))))
   `(shadow ((,class (:foreground ,fg4))))

   `(secondary-selection ((,class (:background ,bg4))))

   `(button ((,class (:foreground ,accent :underline t))))

   ;; dashboard
   `(dashboard-items-face ((,class (:foreground ,fg3))))

   ;; dired
   `(dired-directory ((,class (:foreground ,gray))))
   
   ;; git
   `(diff-added ((,class (:background ,bg1 :foreground ,accent))))
   `(diff-changed ((,class (:background ,bg1 :foreground ,warning))))
   `(diff-removed ((,class (:background ,bg1 :foreground ,error :strike-through t))))
   `(git-gutter:added ((,class (:background ,accent :foreground ,bg0))))
   `(git-gutter:modified ((,class (:background ,warning :foreground ,bg0))))
   `(git-gutter:deleted ((,class (:background ,error :foreground ,bg0))))

   `(message-header-name ((,class (:foreground ,accent))))
   `(message-header-other ((,class (:foreground ,warning))))
   `(message-mml ((,class (:foreground ,accent))))
   `(message-cited-text ((,class (:foreground ,gray))))
   `(message-cited-text-1 ((,class (:foreground ,accent))))
   `(message-cited-text-2 ((,class (:foreground ,fg3))))
   `(message-cited-text-3 ((,class (:foreground ,gray))))
   `(message-cited-text-4 ((,class (:foreground ,accent))))

   ;; Orgmode
   `(org-agenda-calendar-event ((,class (:foreground ,fg4))))
   ;; `(org-agenda-clocking ((,class (:background ,bg4 :foreground ,fg0))))
   
   ;; deadlines
   `(org-upcoming-distant-deadline ((,class (:foreground ,warning :weight medium))))
   `(org-upcoming-deadline ((,class (:foreground ,warning :weight bold))))
   `(org-imminent-deadline ((,class (:foreground ,warning :weight heavy))))
   
   `(org-scheduled-previously ((,class (:foreground ,warning :weight bold))))
   `(org-scheduled-today ((,class (:foreground ,warning :weight normal))))
   `(org-scheduled ((,class (:foreground ,gray :weight normal))))
   
   `(org-time-grid ((,class (:foreground ,bg3))))
   `(org-hide ((,class (:foreground ,bg0h))))
   `(org-dispatcher-highlight ((,class (:weight bold :foreground ,accent :background ,bg2))))
   `(org-tag ((,class (:foreground ,gray))))
   `(org-beamer-tag ((,class (:foreground ,gray :box t))))
   `(org-table ((,class (:foreground ,fg4))))
   `(org-link ((,class (:foreground ,accent))))
   `(org-meta-line ((,class (:foreground ,fg3 :weight semi-light))))
   `(org-drawer ((,class (:foreground ,unspec :inherit org-meta-line))))
   `(org-property-value ((,class (:foreground ,second :inherit org-drawer))))
   `(org-date ((,class (:foreground ,second :underline nil))))
   `(org-document-title ((,class (:foreground ,accent :weight bold))))
   `(org-document-info ((,class (:foreground ,accent))))
   `(persp-selected-face ((,class (:foreground ,fg1))))

   `(sh-quoted-exec ((,class (:foreground ,fg3))))
   ;; doom modeline
   `(doom-modeline-host ((,class (:foreground ,accent))))
   ;; elfeed
   `(elfeed-search-date-face ((,class (:foreground ,bg3))))
   `(elfeed-search-unread-title-face ((,class (:foreground ,accent))))
   `(elfeed-search-title-face ((,class (:foreground ,fg4))))
   `(elfeed-search-feed-face ((,class (:foreground ,fg2))))
   `(elfeed-search-tag-face ((,class (:foreground ,fg3))))
   ;; flycheck
   `(flycheck-info ((,class (:underline (:style wave :color ,accent)))))
   `(flycheck-error ((,class (:underline (:style wave :color ,error)))))
   `(flycheck-warning ((,class (:underline (:style wave :color ,warning)))))
   ;; web-mode
   `(web-mode-html-tag-face ((,class (:foreground ,fg2))))
   `(web-mode-html-tag-bracket-face ((,class (:foreground ,fg3))))
   `(web-mode-html-attr-engine-face ((,class (:foreground ,fg0))))
   `(web-mode-html-attr-name-face ((,class (:foreground ,accent))))
   `(web-mode-html-attr-value-face ((,class (:foreground ,fg4))))
   ;; eshell
   `(eshell-prompt ((,class (:foreground ,accent))))
   ;; vterm
   `(term-color-red ((,class (:foreground ,warning))))
   `(term-color-green ((,class (:foreground ,accent))))
   `(term-color-yellow ((,class (:foreground ,fg4))))
   `(term-color-blue ((,class (:foreground ,fg3))))
   `(term-color-magenta ((,class (:foreground ,fg2))))
   `(term-color-cyan ((,class (:foreground ,fg1))))
   `(term-color-white ((,class (:foreground ,fg0))))
   `(term-color-black ((,class (:foreground ,bg2))))
   ;; `(vterm-color-underline ((,class (:foreground ,error))))
   ;; `(vterm-color-inverse-video ((,class (:foreground ,error))))
   
   ;; other
   `(default ((,class (:background ,bg0h :foreground ,fg2))))
   ;; font-lock
   `(font-lock-keyword-face ((,class (:bold ,class :foreground ,accent))))
   `(font-lock-builtin-face ((,class (:foreground ,fg0 :weight semi-bold))))
   `(font-lock-function-name-face ((,class (:foreground ,fg0 :weight bold))))
   `(font-lock-variable-name-face ((,class (:foreground ,fg1))))
   `(highlight-defined-variable-name-face ((,class (:foreground ,fg1 :weight semi-light))))
   `(font-lock-type-face ((,class (:foreground ,second))))
   `(font-lock-negation-char-face ((,class (:foreground ,fg4))))
   `(font-lock-reference-face ((,class (:foreground ,fg4))))
   `(font-lock-constant-face ((,class (:foreground ,fg0)))) ; second
   `(font-lock-preprocessor-face ((,class (:foreground ,gray))))
   `(font-lock-string-face ((,class (:foreground ,bg4)))) ; gray
   `(font-lock-doc-face ((,class (:foreground ,bg3 :weight light))))
   `(font-lock-comment-face ((,class (:foreground ,bg3 :slant italic))))
   `(font-lock-warning-face ((,class (:foreground ,warning))))

   ;; whitespace
   `(whitespace-space ((,class (:foreground ,bg2 :background ,unspec :weight thin))))
   `(whitespace-hspace ((,class (:inherit whitespace-space :foreground ,warning))))
   `(whitespace-tab ((,class (:inherit whitespace-space))))
   `(whitespace-newline ((,class (:inherit whitespace-space :foreground ,accent))))
   `(whitespace-line ((,class (:foreground ,warning))))
   `(whitespace-empty ((,class (:background ,warning))))
   `(whitespace-trailing ((,class (:foreground ,error :weight bold))))
   `(whitespace-big-indent ((,class (:inherit whitespace-trailing))))
   `(whitespace-space-after-tab ((,class (:inherit whitespace-trailing))))
   `(whitespace-space-before-tab ((,class (:inherit whitespace-trailing))))
   `(whitespace-indentation ((,class (:foreground ,fg0 :weight bold))))
   `(whitespace-missing-newline-at-eof ((,class (:inherit whitespace-empty))))

   `(region ((,class (:background ,bg0s))))
   `(highlight ((,class (:background ,bg2))))
   `(hl-line ((,class (:background  ,bg1))))
   `(fringe ((,class (:background ,bg0 :foreground ,fg3))))
   `(cursor ((,class (:background ,gray))))
   `(isearch ((,class (:bold t :foreground ,accent :background ,bg1))))

   ;; modeline
   `(mode-line ((,class (:bold t :foreground ,fg3 :background ,bg1))))
   `(mode-line-inactive ((,class (:foreground ,fg2 :background ,bg0s))))
   `(mode-line-buffer-id ((,class (:bold t :foreground ,fg1 :background ,unspec))))
   `(mode-line-highlight ((,class (:foreground ,accent :weight bold))))
   `(mode-line-emphasis ((,class (:foreground ,fg0))))

   `(vertical-border ((,class (:foreground ,fg2))))
   `(minibuffer-prompt ((,class (:bold t :foreground ,accent))))
   `(default-italic ((,class (:italic t))))
   `(link ((,class (:foreground ,fg4 :underline t))))
   `(org-code ((,class (:foreground ,fg4))))
   `(org-hide ((,class (:foreground ,fg3))))
   `(org-level-1 ((,class (:foreground ,fg0 :height 1.1 :weight extra-bold))))
   `(org-level-2 ((,class (:inherit org-level-1 :foreground ,fg1 :weight bold))))
   `(org-level-3 ((,class (:inherit org-level-2 :foreground ,fg0 :weight semi-bold))))
   `(org-level-4 ((,class (:inherit org-level-3 :foreground ,fg1 :weight medium))))
   `(org-level-5 ((,class (:inherit org-level-4 :foreground ,fg0 :weight normal))))
   `(org-level-6 ((,class (:inherit org-level-5 :foreground ,fg1 :weight semi-light))))
   `(org-level-7 ((,class (:inherit org-level-6 :foreground ,fg0 :weight light))))
   `(org-level-8 ((,class (:inherit org-level-7 :foreground ,fg1 :weight extra-light))))
   `(outshine-level-1 ((,class (:foreground ,unspec :inherit org-level-1))))
   `(outshine-level-2 ((,class (:foreground ,unspec :inherit org-level-2))))
   `(outshine-level-3 ((,class (:foreground ,unspec :inherit org-level-3))))
   `(outshine-level-4 ((,class (:foreground ,unspec :inherit org-level-4))))
   `(outshine-level-5 ((,class (:foreground ,unspec :inherit org-level-5))))
   `(outshine-level-6 ((,class (:foreground ,unspec :inherit org-level-6))))
   `(outshine-level-7 ((,class (:foreground ,unspec :inherit org-level-7))))
   `(outshine-level-8 ((,class (:foreground ,unspec :inherit org-level-8))))
   `(org-date ((,class (:underline t :foreground ,fg2) )))
   `(org-footnote  ((,class (:underline t :foreground ,fg3))))
   `(org-formula ((,class (:foreground ,accent))))
   `(org-link ((,class (:underline t :foreground ,fg3 ))))
   `(org-special-keyword ((,class (:foreground ,fg1))))
   `(org-block ((,class (:background ,bg0))))
   `(org-block-begin-line ((,class (:foreground ,fg1 :background ,bg0s :extend t))))
   `(org-quote ((,class (:inherit org-block :slant italic))))
   `(org-verse ((,class (:inherit org-block :slant italic))))
   `(org-todo ((,class (:foreground ,accent :weight bold))))
   `(org-done ((,class (:foreground ,bg3 :weight bold))))
   `(org-warning ((,class (:underline t :foreground ,warning))))
   `(org-agenda-structure ((,class (:weight bold :foreground ,fg2 :background ,bg2))))
   `(org-agenda-date ((,class (:foreground ,fg1 :weight semi-bold))))
   `(org-agenda-date-weekend ((,class (:weight medium :foreground ,second))))
   `(org-agenda-date-today ((,class (:weight bold :foreground ,accent))))
   `(org-agenda-done ((,class (:foreground ,bg3))))
   `(org-agenda-dimmed-todo-face ((,class (:foreground ,bg2 :weight thin))))
   `(org-archived ((,class (:background ,bg1 :foreground ,fg3))))
   `(org-scheduled ((,class (:foreground ,fg2))))
   `(org-scheduled-today ((,class (:foreground ,fg1 :weight bold))))
   `(org-ellipsis ((,class (:foreground ,fg0))))
   `(org-verbatim ((,class (:foreground ,second :weight light :background ,bg1))))
   `(org-document-info-keyword ((,class (:foreground ,fg1))))
   `(font-latex-bold-face ((,class (:foreground ,fg3))))
   `(font-latex-italic-face ((,class (:foreground ,fg2 :italic t))))
   `(font-latex-string-face ((,class (:foreground ,gray))))
   `(font-latex-match-reference-keywords ((,class (:foreground ,fg4))))
   `(font-latex-match-variable-keywords ((,class (:foreground ,fg2))))
   `(ido-only-match ((,class (:foreground ,warning))))
   `(org-sexp-date ((,class (:foreground ,fg3))))
   `(ido-first-match ((,class (:foreground ,accent :bold t))))
   ;; ivy, swiper
   `(ivy-current-match ((,class (:foreground ,fg2 :inherit highlight :underline t))))
   `(ivy-minibuffer-match-face-1 ((,class (:background ,bg1))))
   `(ivy-minibuffer-match-face-2 ((,class (:background ,bg2))))
   `(ivy-minibuffer-match-face-3 ((,class (:background ,bg3))))
   `(ivy-minibuffer-match-face-4 ((,class (:background ,bg4))))
   `(swiper-match-face-1 ((,class (:background ,bg1))))
   `(swiper-match-face-2 ((,class (:background ,bg2))))
   `(swiper-match-face-3 ((,class (:background ,bg3))))
   `(swiper-match-face-4 ((,class (:background ,bg4))))
   `(swiper-background-match-face-1 ((,class (:background ,bg1))))
   `(swiper-background-match-face-2 ((,class (:background ,bg2))))
   `(swiper-background-match-face-3 ((,class (:background ,bg3))))
   `(swiper-background-match-face-4 ((,class (:background ,bg4))))
   
   `(gnus-header-content ((,class (:foreground ,accent))))
   `(gnus-header-from ((,class (:foreground ,fg2))))
   `(gnus-header-name ((,class (:foreground ,fg3))))
   `(gnus-header-subject ((,class (:foreground ,fg1 :bold t))))
   `(mu4e-view-url-number-face ((,class (:foreground ,fg3))))
   `(mu4e-cited-1-face ((,class (:foreground ,fg1))))
   `(mu4e-cited-7-face ((,class (:foreground ,fg2))))
   `(mu4e-compose-separator-face ((,class (:foreground ,gray))))
   `(mu4e-header-marks-face ((,class (:foreground ,fg3))))
   `(mu4e-header-title-face ((,class (:foreground ,unspec))))
   `(mu4e-highlight-face ((,class (:foreground ,second :inherit nil))))
   
   `(ffap ((,class (:foreground ,fg3))))
   `(js2-private-function-call ((,class (:foreground ,fg4))))
   `(js2-jsdoc-html-tag-delimiter ((,class (:foreground ,gray))))
   `(js2-jsdoc-html-tag-name ((,class (:foreground ,fg2))))
   `(js2-external-variable ((,class (:foreground ,fg3  ))))
   `(js2-function-param ((,class (:foreground ,fg4))))
   `(js2-jsdoc-value ((,class (:foreground ,gray))))
   `(js2-private-member ((,class (:foreground ,fg2))))
   `(js3-warning-face ((,class (:underline ,accent))))
   `(js3-error-face ((,class (:underline ,warning))))
   `(js3-external-variable-face ((,class (:foreground ,fg2))))
   `(js3-function-param-face ((,class (:foreground ,fg1))))
   `(js3-jsdoc-tag-face ((,class (:foreground ,accent))))
   `(js3-instance-member-face ((,class (:foreground ,fg4))))
   `(warning ((,class (:foreground ,warning))))
   `(ac-completion-face ((,class (:underline t :foreground ,accent))))
   `(info-quoted-name ((,class (:foreground ,fg0))))
   `(info-string ((,class (:foreground ,gray))))
   `(icompletep-determined ((,class :foreground ,fg0)))
   `(undo-tree-visualizer-current-face ((,class :foreground ,fg0)))
   `(undo-tree-visualizer-default-face ((,class :foreground ,fg1)))
   `(undo-tree-visualizer-unmodified-face ((,class :foreground ,fg2)))
   `(undo-tree-visualizer-register-face ((,class :foreground ,fg3)))
   `(slime-repl-inputed-output-face ((,class (:foreground ,fg3))))
   `(trailing-whitespace ((,class :foreground ,unspec :background ,warning)))

   ;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((,class :foreground ,fg0)))
   `(rainbow-delimiters-depth-2-face ((,class :foreground ,fg4)))
   `(rainbow-delimiters-depth-3-face ((,class :foreground ,accent)))
   `(rainbow-delimiters-depth-4-face ((,class :foreground ,fg2)))
   `(rainbow-delimiters-depth-5-face ((,class :foreground ,second)))
   `(rainbow-delimiters-depth-6-face ((,class :foreground ,fg0)))
   `(rainbow-delimiters-depth-7-face ((,class :foreground ,fg4)))
   `(rainbow-delimiters-depth-8-face ((,class :foreground ,accent)))
   `(rainbow-delimiters-depth-9-face ((,class :foreground ,fg2)))
   `(rainbow-delimiters-unmatched-face ((,class :background ,warning :foreground ,fg0)))

   ;; magit
   `(magit-item-highlight ((,class :background ,bg2)))
   `(magit-section-heading        ((,class (:foreground ,accent :weight bold))))
   `(magit-hunk-heading           ((,class (:background ,bg2))))
   `(magit-section-highlight      ((,class (:background ,bg1))))
   `(magit-hunk-heading-highlight ((,class (:background ,bg2))))
   `(magit-diff-context-highlight ((,class (:background ,bg0 :foreground ,fg2))))
   `(magit-diffstat-added   ((,class (:foreground ,accent))))
   `(magit-diffstat-removed ((,class (:foreground ,error))))
   `(magit-diff-hunk-heading ((,class (:foreground ,fg0 :background ,bg1))))
   `(magit-diff-hunk-heading-highlight ((,class (:foreground ,fg0 :background ,bg2 :weight bold))))
   `(magit-diff-hunk-heading-selection ((,class (:foreground ,accent :background ,bg4 :weight bold))))
   `(magit-process-ok ((,class (:foreground ,fg1 :weight bold))))
   `(magit-process-ng ((,class (:foreground ,warning :weight bold))))
   `(magit-branch-local ((,class (:foreground ,accent :weight bold))))
   `(magit-branch-remote ((,class (:foreground ,fg4 :weight bold))))
   `(magit-log-author ((,class (:foreground ,fg2))))
   `(magit-hash ((,class (:foreground ,fg1))))
   `(magit-diff-file-header ((,class (:foreground ,fg1 :background ,bg2))))

   `(lazy-highlight ((,class (:foreground ,fg1 :background ,bg2))))
   `(term ((,class (:foreground ,fg0 :background ,bg0))))
   `(term-color-black ((,class (:foreground ,bg2 :background ,bg2))))
   `(term-color-blue ((,class (:foreground ,fg1 :background ,fg1))))
   `(term-color-red ((,class (:foreground ,accent :background ,bg2))))
   `(term-color-green ((,class (:foreground ,fg3 :background ,bg2))))
   `(term-color-yellow ((,class (:foreground ,fg2 :background ,fg2))))
   `(term-color-magenta ((,class (:foreground ,fg0 :background ,fg0))))
   `(term-color-cyan ((,class (:foreground ,gray :background ,gray))))
   `(term-color-white ((,class (:foreground ,fg1 :background ,fg1))))
   `(helm-header ((,class (:foreground ,fg1 :background ,bg0 :underline nil :box nil))))
   `(helm-source-header ((,class (:foreground ,accent :background ,bg0 :underline nil :weight bold))))
   `(helm-selection ((,class (:background ,bg1 :underline nil))))
   `(helm-selection-line ((,class (:background ,bg1))))
   `(helm-visible-mark ((,class (:foreground ,bg0 :background ,bg2))))
   `(helm-candidate-number ((,class (:foreground ,bg0 :background ,fg0))))
   `(helm-separator ((,class (:foreground ,fg3 :background ,bg0))))
   `(helm-time-zone-current ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-time-zone-home ((,class (:foreground ,fg3 :background ,bg0))))
   `(helm-buffer-not-saved ((,class (:foreground ,fg3 :background ,bg0))))
   `(helm-buffer-process ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-buffer-saved-out ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-buffer-size ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-ff-directory ((,class (:foreground ,fg1 :background ,bg0 :weight bold))))
   `(helm-ff-file ((,class (:foreground ,fg0 :background ,bg0 :weight normal))))
   `(helm-ff-executable ((,class (:foreground ,fg2 :background ,bg0 :weight normal))))
   `(helm-ff-invalid-symlink ((,class (:foreground ,error :background ,bg0 :weight bold))))
   `(helm-ff-symlink ((,class (:foreground ,accent :background ,bg0 :weight bold))))
   `(helm-ff-prefix ((,class (:foreground ,bg0 :background ,accent :weight normal))))
   `(helm-grep-cmd-line ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-grep-file ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-grep-finish ((,class (:foreground ,fg1 :background ,bg0))))
   `(helm-grep-lineno ((,class (:foreground ,fg0 :background ,bg0))))
   `(helm-grep-match ((,class (:foreground ,unspec :background ,unspec :inherit helm-match))))
   `(helm-grep-running ((,class (:foreground ,fg1 :background ,bg0))))
   `(helm-moccur-buffer ((,class (:foreground ,fg1 :background ,bg0))))
   `(helm-source-go-package-godoc-description ((,class (:foreground ,gray))))
   `(helm-bookmark-w3m ((,class (:foreground ,fg3))))
   `(company-echo-common ((,class (:foreground ,bg0 :background ,fg0))))
   `(company-preview ((,class (:background ,bg0 :foreground ,fg2))))
   `(company-preview-common ((,class (:foreground ,bg1 :foreground ,fg2))))
   `(company-preview-search ((,class (:foreground ,fg3 :background ,bg0))))
   `(company-scrollbar-bg ((,class (:background ,bg2))))
   `(company-scrollbar-fg ((,class (:foreground ,accent))))
   `(company-tooltip ((,class (:foreground ,fg1 :background ,bg1 :bold t))))
   `(company-tooltop-annotation ((,class (:foreground ,fg4))))
   `(company-tooltip-common ((,class ( :foreground ,fg2))))
   `(company-tooltip-common-selection ((,class (:foreground ,gray))))
   `(company-tooltip-mouse ((,class (:inherit highlight))))
   `(company-tooltip-selection ((,class (:background ,bg2 :foreground ,fg2))))
   `(company-template-field ((,class (:inherit region))))
   `(web-mode-builtin-face ((,class (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face ((,class (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face ((,class (:inherit ,font-lock-constant-face))))
   `(web-mode-keyword-face ((,class (:foreground ,accent))))
   `(web-mode-doctype-face ((,class (:inherit ,font-lock-comment-face))))
   `(web-mode-function-name-face ((,class (:inherit ,font-lock-function-name-face))))
   `(web-mode-string-face ((,class (:foreground ,gray))))
   `(web-mode-type-face ((,class (:inherit ,font-lock-type-face))))
   `(web-mode-html-attr-name-face ((,class (:foreground ,fg1))))
   `(web-mode-html-attr-equal-face ((,class (:foreground ,fg2))))
   `(web-mode-html-attr-value-face ((,class (:foreground ,accent))))
   `(web-mode-warning-face ((,class (:inherit ,font-lock-warning-face))))
   `(web-mode-html-tag-face ((,class (:foreground ,fg0))))
   `(jde-java-font-lock-package-face ((t (:foreground ,fg2))))
   `(jde-java-font-lock-public-face ((t (:foreground ,accent))))
   `(jde-java-font-lock-private-face ((t (:foreground ,accent))))
   `(jde-java-font-lock-constant-face ((t (:foreground ,fg4))))
   `(jde-java-font-lock-modifier-face ((t (:foreground ,fg1))))
   `(jde-jave-font-lock-protected-face ((t (:foreground ,accent))))
   `(jde-java-font-lock-number-face ((t (:foreground ,fg2))))
   `(yas-field-highlight-face ((t (:background ,bg0s)))))
  ;; Legacy
  (if (< emacs-major-version 22)
      (custom-theme-set-faces
       theme-name
       `(show-paren-match-face ((,class (:background ,warning))))) ;; obsoleted in 22.1, removed 2016
    (custom-theme-set-faces
     theme-name
     `(show-paren-match ((,class (:foreground ,bg0 :background ,gray))))
     `(show-paren-mismatch ((,class (:foreground ,bg0 :background ,warning))))))
  ;; emacs >= 26.1
  (when (>= emacs-major-version 26)
    (custom-theme-set-faces
     theme-name
     `(line-number ((t (:inherit fringe))))
     `(line-number-current-line ((t (:inherit fringe :foreground ,fg4 :weight bold))))))

  ;; emacs >= 27.1
  (when (>= emacs-major-version 27)
    (custom-theme-set-faces
     theme-name
     `(tab-line              ((,class (:background ,bg1 :foreground ,fg3))))
     `(tab-line-tab          ((,class (:inherit tab-line))))
     `(tab-line-tab-inactive ((,class (:background ,bg1 :foreground ,fg3))))
     `(tab-line-tab-current  ((,class (:background ,bg0 :foreground ,fg0))))
     `(tab-line-highlight    ((,class (:background ,bg0 :foreground ,fg1))))))
  (when (>= emacs-major-version 28)
    (custom-theme-set-faces
     theme-name
     `(line-number ((t (:inherit fringe))))
     `(line-number-current-line ((t (:inherit fringe :foreground ,fg4 :weight bold))))))
  ;; emacs >= 27.1
  (when (>= emacs-major-version 27)
    (custom-theme-set-faces
     theme-name
     `(tab-line              ((,class (:background ,bg1 :foreground ,fg3))))
     `(tab-line-tab          ((,class (:inherit tab-line))))
     `(tab-line-tab-inactive ((,class (:background ,bg1 :foreground ,fg3))))
     `(tab-line-tab-current  ((,class (:background ,bg0 :foreground ,fg0))))
     `(tab-line-highlight    ((,class (:background ,bg0 :foreground ,fg1))))))
  (when (>= emacs-major-version 28)
    (custom-theme-set-faces
     theme-name
     `(tab-line-tab-modified ((,class (:foreground ,error :weight bold))))))
  (when (boundp 'font-lock-regexp-face)
    (custom-theme-set-faces
     theme-name
     `(font-lock-regexp-face ((,class (:inherit font-lock-string-face :underline t)))))))

(provide 'graybox-common)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; graybox-common.el ends here
