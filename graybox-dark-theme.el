;;; graybox-dark-theme.el --- Theme

;; Copyright (C) 2023 , Adrian Bacaliu

;; Author: Adrian Bacaliu
;; Version: 0.1
;; Package-Requires: ((emacs "24.1"))
;; Created with ThemeCreator, https://github.com/mswift42/themecreator.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:
;;; graybox-dark theme created by Adrian Bacaliu in 2023

;;; Code:

(require 'graybox-common)
(deftheme graybox-dark)
(let ((class '((class color) (min-colors 89)))
      (fg0  "#fbf1c7")
      (fg1  "#ebdbb2")
      (fg2  "#d5c4a1")
      (fg3  "#bdae93")
      (fg4  "#a89984")
      (gray "#928374")
      (bg4  "#7c6f64")
      (bg3  "#665c54")
      (bg2  "#504945")
      (bg1  "#3c3836")
      (bg0s "#32302f")
      (bg0  "#282828")
      (bg0h "#1d2021")

      ;;; choose your favorite accent-color
      ;; (accent "#fe8019") ; orange
      (second "#83a598") ; blue
      (accent "#8ec07c") ; aqua

      (error     "#fb4934")
      (warning   "#fabd2f")
      
      (unspec   (when (>= emacs-major-version 29) 'unspecified)))

  (graybox-set-faces 'graybox-dark))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'graybox-dark)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; graybox-dark-theme.el ends here
