;;; graybox-light-theme.el --- Theme

;; Copyright (C) 2023 , Adrian Bacaliu

;; Author: Adrian Bacaliu
;; Version: 0.1
;; Package-Requires: ((emacs "24.1"))
;; Created with ThemeCreator, https://github.com/mswift42/themecreator.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:
;;; graybox-light theme created by Adrian Bacaliu in 2023

;;; Code:

(require 'graybox-common)
(deftheme graybox-light)
(let ((class '((class color) (min-colors 89)))
      (fg0  "#282828")
      (fg1  "#3c3836")
      (fg2  "#504945")
      (fg3  "#665c54")
      (fg4  "#7c6f64")
      (gray "#928374")
      (bg4  "#a89984")
      (bg3  "#bdae93")
      (bg2  "#d5c4a1")
      (bg1  "#ebdbb2")
      (bg0s "#f2e5bc")
      (bg0  "#fbf1c7")
      (bg0h "#f9f5d7")

      ;; choose one suitable accent-color:
      ;; (accent "#af3a03") ; orange
      (second "#076678") ; blue
      (accent "#427b5a") ; aqua

      (error   "#9d0006")
      (warning "#b57614")
      
      (unspec   (when (>= emacs-major-version 29) 'unspecified)))
  (graybox-set-faces 'graybox-light))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'graybox-light)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; graybox-light-theme.el ends here
