- [Screenshots](#orgdfff93d)
- [Experimental warning](#org9a74ff3)
- [How to install](#orgf49d12e)
- [Philosophy](#org22fb4f9)

With graybox-themes I want to provide a color-less emacs-theme. By using the gruvbox-colors the overall tint is brown-ish which is easy to the eyes too.  


<a id="orgdfff93d"></a>

# Screenshots

![img](assets/2023-09-19T10.00.36 57 Emacs -- screen.png "graybox-dark")  

![img](assets/2023-09-19T10.00.50 01 Emacs -- screen.png "graybox-light")  


<a id="org9a74ff3"></a>

# Experimental warning

This theme is highly in development. I don't have any plan. I just add more and more font-faces when I find bad colors in my daily use of Emacs.  

This README is very bad at this point. I will add more information later.  


<a id="orgf49d12e"></a>

# How to install

```elisp
(use-package graybox-themes
  :elpaca (:host gitlab :repo "Bacaliu/graybox-themes")
  :bind (("<f6>" . (lambda () (interactive) (load-theme 'graybox-dark t)))
         ("<f7>" . (lambda () (interactive) (load-theme 'graybox-light t))))
  :config
  (load-theme 'graybox-dark t))
```

I find it useful to bind `<f6>` and `<f7>` to load the dark and light theme. Somehow `(load-theme 'graybox-dark t)` does not work. So after starting Emacs I have to press the button myself.  


<a id="org22fb4f9"></a>

# Philosophy

Other than the several gray-ish colors only three actual *colorful* colors are used. Red for errors, yellow for warnings and aqua (cyan) for a few things, like keywords.  
After using such limited colors for only a few days I cannot use any more colorful theme anymore. It always feels like visual noise, like I look at an exploded clown.